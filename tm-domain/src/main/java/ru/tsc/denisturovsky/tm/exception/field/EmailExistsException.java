package ru.tsc.denisturovsky.tm.exception.field;

import ru.tsc.denisturovsky.tm.exception.user.AbstractUserException;

public final class EmailExistsException extends AbstractUserException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
