package ru.tsc.denisturovsky.tm.exception.user;

public final class UserLockedException extends AbstractUserException {

    public UserLockedException() {
        super("Error! User is locked, contact your administrator.");
    }

}
