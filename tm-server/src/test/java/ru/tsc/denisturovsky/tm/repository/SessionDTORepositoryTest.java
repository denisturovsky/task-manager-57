package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.SessionTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class SessionDTORepositoryTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(USER_ID, USER_SESSION3));
            entityManager.getTransaction().commit();
            @Nullable final SessionDTO session = repository.findOneById(USER_ID, USER_SESSION3.getId());
            Assert.assertNotNull(session);
            Assert.assertEquals(USER_SESSION3.getId(), session.getId());
            Assert.assertEquals(USER_ID, session.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_ID);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Before
    public void before() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(USER_ID, USER_SESSION1);
            repository.add(USER_ID, USER_SESSION2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_ID);
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, repository.getSize(USER_ID));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        Assert.assertFalse(repository.existsById(USER_ID, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_ID, USER_SESSION1.getId()));
        entityManager.close();
    }

    @Test
    public void findAllByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<SessionDTO> sessions = repository.findAll(USER_ID);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(USER_ID, session.getUserId()));
        entityManager.close();
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        Assert.assertNull(repository.findOneById(USER_ID, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = repository.findOneById(USER_ID, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
        entityManager.close();
    }

    @NotNull
    public ISessionDTORepository getRepository() {
        return CONTEXT.getBean(ISessionDTORepository.class);
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        Assert.assertEquals(2, repository.getSize(USER_ID));
        entityManager.close();
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(USER_ID, USER_SESSION2);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_SESSION2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}